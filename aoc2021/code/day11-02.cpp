#include "common.h"

struct oct
{
    s8 Charge;
    u8 Flashed;
};

int
main(void)
{
    char *FileName = "day11-01.txt";
    file_result File = ReadFile(FileName);
    if(!File.FileSize)
    {
        fprintf(stderr, "Failed to open %s.\n", FileName);
        return(-1);
    }

    memory_arena Arena_ = CreateMemoryArena(Kilobytes(64));
    memory_arena *Arena = &Arena_;

    s32 GridWidth = 10;
    s32 GridHeight = 10;
    u32 OctsCount = GridWidth*GridHeight; 
    oct *Octs = PushArray(Arena, OctsCount, oct);
    ZeroSize(Octs, sizeof(*Octs)*OctsCount);

    oct *InsertOct = Octs;
    for(u8 *At = File.Contents; *At; ++At)
    {
        if((At[0] >= '0') && (At[0] <= '9'))
        {
            InsertOct->Charge = At[0] - '0';
            ++InsertOct;
        }
    }

    u32 TickCount = 0;
    b32 Simulate = true;
    while(Simulate)
    {
        for(s32 Y = 0; Y < GridHeight; ++Y)
        {
            for(s32 X = 0; X < GridWidth; ++X)
            {
                oct *Oct = Octs + (Y * GridWidth) + X;
                ++Oct->Charge;
                Oct->Flashed = false;
            }
        }

        u32 LocalFlashCount = 0;
        b32 Flashed = false;
        do
        {
            Flashed = false;
            for(s32 Y = 0; Y < GridHeight; ++Y)
            {
                for(s32 X = 0; X < GridWidth; ++X)
                {
                    oct *Oct = Octs + (Y * GridWidth) + X;
                    if((Oct->Charge > 9) && !Oct->Flashed)
                    {
                        Oct->Charge = 0;
                        Oct->Flashed = true;
                        Flashed = true;
                        ++LocalFlashCount;

                        s32 YMin = Y - 1;
                        s32 YMax = Y + 1;
                        s32 XMin = X - 1;
                        s32 XMax = X + 1;

                        for(s32 LocalY = YMin; LocalY <= YMax; ++LocalY)
                        {
                            for(s32 LocalX = XMin; LocalX <= XMax; ++LocalX)
                            {
                                if((LocalY >= 0) && (LocalY < GridHeight) &&
                                   (LocalX >= 0) && (LocalX < GridWidth))
                                {
                                    oct *Oct = Octs + (LocalY * GridWidth) + LocalX;
                                    if(!Oct->Flashed)
                                    {
                                        ++Oct->Charge;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } while(Flashed);

        if(LocalFlashCount == OctsCount)
        {
            Simulate = false;
            break;
        }
        
        ++TickCount;
    }

    printf("Result: %d\n", TickCount + 1);

    return(0);
}
