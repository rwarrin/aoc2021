#include "common.h"

union v2
{
    struct
    {
        u16 x, y;
    };
    u16 E[2];
};

union line
{
    struct
    {
        v2 P0;
        v2 P1;
    };
    v2 E[2];
};

int
main(void)
{
    char *FileName = "day05-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    memory_arena Arena_ = CreateMemoryArena(Megabytes(2));
    memory_arena *Arena = &Arena_;

    u32 LineCount = 0;
    u32 MaxLineCount = 1000;
    line *Lines = PushArray(Arena, MaxLineCount, line);

    u32 MaxXCoordinate = 0;
    u32 MaxYCoordinate = 0;
    line ReadLine = {0};
    while(fscanf(File, "%hu,%hu -> %hu,%hu", &ReadLine.P0.x, &ReadLine.P0.y, &ReadLine.P1.x, &ReadLine.P1.y) == 4)
    {
        MaxXCoordinate = MAX(MaxXCoordinate, MAX(ReadLine.P0.x, ReadLine.P1.x));
        MaxYCoordinate = MAX(MaxYCoordinate, MAX(ReadLine.P0.y, ReadLine.P1.y));
        Lines[LineCount++] = ReadLine;
    }

    // NOTE(rick): Filter out diagonal lines
    for(u32 LineIndex = 0; LineIndex < LineCount; )
    {
        line *Line = Lines + LineIndex;
        if((Line->P0.x == Line->P1.x) ||
           (Line->P0.y == Line->P1.y))
        {
            ++LineIndex;
        }
        else
        {
            line Temp = *Line;
            *Line = Lines[--LineCount];
            Lines[LineCount] = Temp;
        }
    }

    MaxXCoordinate += 1;
    MaxYCoordinate += 1;
    u32 GridSize = MaxXCoordinate*MaxYCoordinate;
    u16 *Grid = PushArray(Arena, GridSize, u16);

    for(u32 LineIndex = 0; LineIndex < LineCount; ++LineIndex)
    {
        line *Line = Lines + LineIndex;

        u16 MinX = MIN(Line->P0.x, Line->P1.x);
        u16 MaxX = MAX(Line->P0.x, Line->P1.x);
        u16 MinY = MIN(Line->P0.y, Line->P1.y);
        u16 MaxY = MAX(Line->P0.y, Line->P1.y);

        u16 *Row = Grid + (MinY * MaxXCoordinate);
        for(u32 Y = MinY; Y <= MaxY; ++Y)
        {
            u16 *Point = Row + MinX;
            for(u32 X = MinX; X <= MaxX; ++X)
            {
                *Point = *Point + 1;
                ++Point;
            }
            Row += MaxXCoordinate;
        }
    }

    u32 OverlapPointCount = 0;
    u16 *Point = Grid;
    for(u32 PointIndex = 0; PointIndex < GridSize; ++PointIndex)
    {
        OverlapPointCount += (*Point++ > 1 ? 1 : 0);
    }

    printf("Result: %d\n", OverlapPointCount);

    return(0);
}
