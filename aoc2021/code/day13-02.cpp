#include "common.h"

enum fold_type
{
    FoldType_Horizontal = 'y',
    FoldType_Vertical = 'x',
};

inline void
PrintGrid(u32 Width, u32 Height, u8 *Grid, u32 FullHeight, u32 FullWidth)
{
    for(u32 Y = 0; Y < Height; ++Y)
    {
        for(u32 X = 0; X < Width; ++X)
        {
            u8 *Byte = (Grid + (Y * FullHeight) + X);
            printf("%c", *Byte ? '#' : '.');
        }
        printf("\n");
    }
    printf("\n");
}

int
main(void)
{
    char *FileName = "day13-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to read %s\n", FileName);
        return(-1);
    }

    memory_arena Arena_ = CreateMemoryArena(Megabytes(2));
    memory_arena *Arena = &Arena_;

    u32 PointsCount = 0;
    u32 MaxPointsCount = 1000;
    u32 *Points = PushArray(Arena, MaxPointsCount, u32);

    u32 FoldsCount = 0;
    u32 MaxFoldsCount = 32;
    u32 *Folds = PushArray(Arena, MaxFoldsCount, u32);

    u32 FullWidth = 0;
    u32 FullHeight = 0;

    char LineBuffer[32] = {0};
    while(fgets(LineBuffer, ArrayCount(LineBuffer), File) != 0)
    {
        if((LineBuffer[0] >= '0') && (LineBuffer[0] <= '9'))
        {
            u32 X = 0;
            u32 Y = 0;
            sscanf(LineBuffer, "%u,%u", &X, &Y);

            FullWidth = MAX(FullWidth, X + 1);
            FullHeight = MAX(FullHeight, Y + 1);

            Assert((PointsCount + 1) < MaxPointsCount);
            *(Points + PointsCount++) = (u32)(((u16)X << 16) | ((u16)Y << 0));
        }
        else if(LineBuffer[0] == 'f')
        {
            for(char *At = LineBuffer; *At; ++At)
            {
                if(*At == '=')
                {
                    char FoldDirection = *(At - 1);
                    u32 FoldPosition = 0;
                    sscanf(At + 1, "%u", &FoldPosition);

                    Assert((FoldsCount + 1) < MaxFoldsCount);
                    *(Folds + FoldsCount++) = (u32)((FoldDirection << 16) | (FoldPosition));
                    break;
                }
            }
        }
    }

    u8 *Grid = PushArray(Arena, FullWidth*FullHeight, u8);

    for(u32 PointIndex = 0; PointIndex < PointsCount; ++PointIndex)
    {
        u32 *Point = Points + PointIndex;

        u16 X = *Point >> 16;
        u16 Y = *Point & 0xFFFF;

        Assert((X <= FullWidth) && (X >= 0));
        Assert((Y <= FullHeight) && (Y >= 0));

        *(Grid + (Y * FullWidth) + X) = '#';
    }

    u32 Width = FullWidth;
    u32 Height = FullHeight;
    for(u32 FoldIndex = 0; FoldIndex < FoldsCount; ++FoldIndex)
    {
        u32 *Fold = Folds + FoldIndex;

        u8 FoldType = (u8)(*Fold >> 16);
        u16 FoldPoint = *Fold & 0xFFFF;

        if(FoldType == FoldType_Horizontal)
        {
            u8 *YDest = Grid;
            u8 *YSrc = Grid + ((FoldPoint*2) * FullWidth);
            for(u32 Y = 0; Y < FoldPoint; ++Y)
            {
                u8 *Src = YSrc;
                u8 *Dest = YDest;
                for(u32 X = 0; X < Width; ++X)
                {
                    if(*Src)
                    {
                        *Dest = *Src;
                        *Src = 0;
                    }

                    ++Src, ++Dest;
                }

                YDest += FullWidth;
                YSrc -= FullWidth;
            }

            Height = FoldPoint;
        }
        else if(FoldType == FoldType_Vertical)
        {
            for(u32 Y = 0; Y < Height; ++Y)
            {
                u8 *Dest = (Grid + (Y * FullWidth));
                u8 *Src = (Grid + (Y * FullWidth) + (FoldPoint*2));
                for(u32 X = 0; X < FoldPoint; ++X)
                {
                    if(*Src)
                    {
                        *Dest = *Src;
                        *Src = 0;
                    }

                    --Src, ++Dest;
                }
            }

            Width = FoldPoint;
        }
        else
        {
            InvalidCodePath;
        }
    }

    PrintGrid(Width, Height, Grid, FullWidth, FullHeight);

    return(0);
}
