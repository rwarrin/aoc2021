#include "common.h"

inline b32
IsChunkOpener(char C)
{
    b32 Result = ((C == '(') ||
                  (C == '[') ||
                  (C == '{') ||
                  (C == '<'));
    return(Result);
}

inline b32
IsChunkCloser(char C)
{
    b32 Result = ((C == ')') ||
                  (C == ']') ||
                  (C == '}') ||
                  (C == '>'));
    return(Result);
}

inline char
GetChunkCloserFromOpener(char C)
{
    char Result = 0;
    switch(C)
    {
        case '(': { Result = ')'; } break;
        case '[': { Result = ']'; } break;
        case '{': { Result = '}'; } break;
        case '<': { Result = '>'; } break;
        InvalidDefaultCase;
    }
    return(Result);
}

int
main(void)
{
    char *FileName = "day10-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open file %s\n", FileName);
        return(-1);
    }

    u32 SyntaxErrorScore = 0;

    const s32 MaxStackSize = 128;
    char Stack[MaxStackSize] = {0};
    s32 StackSize = 0;

    char ReadBuffer[256] = {0};
    while(fgets(ReadBuffer, ArrayCount(ReadBuffer), File) != 0)
    {
        ZeroSize(Stack, sizeof(char)*MaxStackSize);
        StackSize = 0;
        for(char *At = ReadBuffer; *At; ++At)
        {
            if(IsChunkOpener(At[0]))
            {
                Assert((StackSize + 1) < MaxStackSize);
                Stack[StackSize++] = *At;
            }
            else if(IsChunkCloser(At[0]))
            {
                Assert(StackSize > 0);
                char Closer = GetChunkCloserFromOpener(Stack[--StackSize]);
                if(At[0] != Closer)
                {
                    u32 Value = 0;
                    switch(At[0])
                    {
                        case ')': { Value = 3; } break;
                        case ']': { Value = 57; } break;
                        case '}': { Value = 1197; } break;
                        case '>': { Value = 25137; } break;
                        InvalidDefaultCase;
                    }
                    printf("%s, %c\n", ReadBuffer, At[0]);
                    SyntaxErrorScore += Value;
                    break;
                }
            }
        }
    }

    printf("Result: %u\n", SyntaxErrorScore);

    return(0);
}
