#include "common.h"

int
main(void)
{
    char *FileName = "day07-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    u32 CrabPositionCount = 0;
    u32 CrabPositions[1001] = {0};
    u32 InputPosition = 0;
    while(fscanf(File, "%u,", &InputPosition) == 1)
    {
        Assert((CrabPositionCount + 1) < ArrayCount(CrabPositions));
        CrabPositions[CrabPositionCount++] = InputPosition;
    }

    for(u32 i = 0; i < CrabPositionCount; ++i)
    {
        for(u32 j = i + 1; j < CrabPositionCount - 1; ++j)
        {
            if(CrabPositions[j] < CrabPositions[i])
            {
                u32 Temp = CrabPositions[j];
                CrabPositions[j] = CrabPositions[i];
                CrabPositions[i] = Temp;
            }
        }
    }

    u32 MedianPosition = CrabPositions[CrabPositionCount / 2];

    u32 FuelCost = 0;
    for(u32 CrabIndex = 0; CrabIndex < CrabPositionCount; ++CrabIndex)
    {
        FuelCost += (u32)AbsoluteValue((s32)CrabPositions[CrabIndex] - (s32)MedianPosition);
    }

    printf("Result: %u\n", FuelCost);

    return(0);
}
