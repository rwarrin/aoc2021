#include "common.h"

inline u32
CalculateFuel(u32 Distance)
{
    u32 Result = (u32)(0.5f*(Distance*(Distance + 1)));
    return(Result);
}

int
main(void)
{
    char *FileName = "day07-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    u32 CrabPositionCount = 0;
    u32 CrabPositions[1001] = {0};
    u32 InputPosition = 0;
    f32 AveragePosition = 0.0f;
    while(fscanf(File, "%u,", &InputPosition) == 1)
    {
        Assert((CrabPositionCount + 1) < ArrayCount(CrabPositions));
        CrabPositions[CrabPositionCount++] = InputPosition;
        AveragePosition +=  InputPosition;
    }
    AveragePosition /= (f32)CrabPositionCount;

    u32 PositionFloor = (u32)AveragePosition;
    u32 PositionCeil = (u32)(AveragePosition + 0.5f);

    u32 FloorFuelCost = 0;
    u32 CeilFuelCost = 0;
    for(u32 CrabIndex = 0; CrabIndex < CrabPositionCount; ++CrabIndex)
    {
        FloorFuelCost += CalculateFuel((u32)AbsoluteValue((s32)CrabPositions[CrabIndex] - (s32)PositionFloor));
        CeilFuelCost += CalculateFuel((u32)AbsoluteValue((s32)CrabPositions[CrabIndex] - (s32)PositionCeil));
    }

    u32 MinFuelCost = MIN(FloorFuelCost, CeilFuelCost);
    printf("Result: %u\n", MinFuelCost);

    return(0);
}
