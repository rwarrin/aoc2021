#include <stdio.h>
#include <limits.h>
#include "common.h"

int
main(void)
{
    char *FileName = "day01-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    memory_arena Arena = CreateMemoryArena(Kilobytes(32));
    s16 *Values = PushArray(&Arena, 2000, s16);

    u32 ValueCount = 0;
    char ReadBuffer[32] = {0};
    while(fgets(ReadBuffer, ArrayCount(ReadBuffer), File) != 0)
    {
        s32 Value = atoi(ReadBuffer);
        *(Values + ValueCount++) = (s16)Value;
    }

    s32 IncreaseCount = 0;
    for(u32 Index = 0, Limit = ValueCount - 3; Index <= Limit; ++Index)
    {
        IncreaseCount += (Values[Index] < Values[Index + 3]) ? 1 : 0;
    }

    printf("Result: %d\n", IncreaseCount);

    return(0);
}
