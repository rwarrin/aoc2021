#include "common.h"

struct pair_table
{
    u64 Pairs[26][26];
};

int
main(void)
{
    char *FileName = "day14-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to read %s\n", FileName);
        return(-1);
    }

    char Seed[32] = {0};
    char Rules[26][26] = {0};
    pair_table PairTables[2] = {0};
    pair_table *CurrentTable = &PairTables[0];
    pair_table *PreviousTable = &PairTables[1];

    u32 Line = 0;
    char LineBuffer[32] = {0};
    while(fgets(LineBuffer, ArrayCount(LineBuffer), File) != 0)
    {
        if(Line == 0)
        {
            Copy(LineBuffer, Seed, ArrayCount(LineBuffer));
        }
        else if((LineBuffer[0] >= 'A') && (LineBuffer[0] <= 'Z'))
        {
            u8 *At = (u8 *)LineBuffer;
            u8 Row = *At++;
            u8 Col = *At++;

            while(At[0] != '>') { ++At; }
            At += 2;

            u8 Value = *At;
            Rules[Row - 'A'][Col - 'A'] = Value;
        }

        ++Line;
    }

    ZeroSize(CurrentTable, sizeof(*CurrentTable));
    char *SeedEnd = Seed;
    for(u32 SeedIndex = 1; SeedIndex < ArrayCount(Seed); ++SeedIndex)
    {
        if((Seed[SeedIndex] >= 'A') && (Seed[SeedIndex] <= 'Z'))
        {
            u8 Row = Seed[SeedIndex - 1] - 'A';
            u8 Col = Seed[SeedIndex] - 'A';
            ++CurrentTable->Pairs[Row][Col];
            ++SeedEnd;
        }
    }

    u64 Histogram[26] = {0};
    for(char *At = Seed; *At; ++At)
    {
        if((At[0] >= 'A') && (At[0] <= 'Z'))
        {
            ++Histogram[At[0] - 'A'];
        }
    }

    u32 StepCount = 40;
    for(u32 Step = 0; Step < StepCount; ++Step)
    {
        pair_table *Temp = CurrentTable;
        CurrentTable = PreviousTable;
        PreviousTable = Temp;

        ZeroSize(CurrentTable, sizeof(*CurrentTable));

        for(u32 R = 0; R < 26; ++R)
        {
            for(u32 C = 0; C < 26; ++C)
            {
                u64 PairCount = PreviousTable->Pairs[R][C];
                u8 Rule = Rules[R][C];

                CurrentTable->Pairs[R][Rule - 'A'] += PairCount;
                CurrentTable->Pairs[Rule - 'A'][C] += PairCount;
                Histogram[Rule - 'A'] += PairCount;
            }
        }
    }

    u64 MinCount = 0xFFFFFFFFFFFFFFFF;
    u64 MaxCount = 0;
    for(u32 HistogramIndex = 0; HistogramIndex < ArrayCount(Histogram); ++HistogramIndex)
    {
        if(Histogram[HistogramIndex])
        {
            u64 Count = Histogram[HistogramIndex];
            MinCount = MIN(MinCount, Count);
            MaxCount = MAX(MaxCount, Count);
        }
    }

    u64 Result = MaxCount - MinCount;
    printf("%llu - %llu = %llu\n", MaxCount, MinCount, Result);

    return(0);
}
