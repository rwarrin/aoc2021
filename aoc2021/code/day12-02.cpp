#include "common.h"

enum vertex_type
{
    VertexType_None,

    VertexType_Small,
    VertexType_Big,
};

struct vertex
{
    vertex_type Type;
    b32 Visited;

    char Label[8];

    u32 EdgeCount;
    vertex *Edges[16];

    vertex *Next;

};

inline vertex *
GetVertexByLabel(char *Label, vertex *Verts, u32 VertCount)
{
    vertex *Result = 0;

    vertex *TestVert = Verts;
    for(u32 VertIndex = 0; VertIndex < VertCount; ++VertIndex, ++TestVert)
    {
        if(StringCompare(TestVert->Label, Label) == 0)
        {
            Result = TestVert;
            break;
        }
    }

    return(Result);
}

inline vertex *
CreateVertex(char *Label, vertex *Verts, u32 Index)
{
    vertex *Result = Verts + Index;
    Result->Visited = false;
    StringCopy(Label, Result->Label, ArrayCount(Result->Label));

    if((Label[0] >= 'A') && (Label[0] <= 'Z'))
    {
        Result->Type = VertexType_Big;
    }
    else
    {
        Result->Type = VertexType_Small;
    }

    return(Result);
}

inline void
DepthFirstSearch(vertex *Node, vertex *Target, vertex *StartVert, u32 *Count, b32 TwiceUsed)
{
    if(Node == Target)
    {
        (*Count)++;
        return;
    }

    for(u32 EdgeIndex = 0; EdgeIndex < Node->EdgeCount; ++EdgeIndex)
    {
        vertex *Vertex = Node->Edges[EdgeIndex];
        if(Vertex->Type == VertexType_Big)
        {
            DepthFirstSearch(Vertex, Target, StartVert, Count, TwiceUsed);
        }
        else if(!Vertex->Visited)
        {
            Vertex->Visited = true;
            DepthFirstSearch(Vertex, Target, StartVert, Count, TwiceUsed);
            Vertex->Visited = false;
        }
        else if(!TwiceUsed && Vertex != StartVert)
        {
            DepthFirstSearch(Vertex, Target, StartVert, Count, true);
        }
    }
}

int
main(void)
{
    char *FileName = "day12-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to read %s\n", FileName);
        return(-1);
    }

    memory_arena Arena_ = CreateMemoryArena(Kilobytes(8));
    memory_arena *Arena = &Arena_;

    u32 VertexCount = 0;
    u32 MaxVertexCount = 32;
    vertex *Vertices = PushArray(Arena, MaxVertexCount, vertex);

    char LineBuffer[32] = {0};
    while(fgets(LineBuffer, ArrayCount(LineBuffer), File) != 0)
    {
        char *RightLabel = 0;
        char *LeftLabel = LineBuffer;
        for(char *At = LineBuffer; *At; ++At)
        {
            if(At[0] == '-')
            {
                *At++ = 0;
                RightLabel = At;
            }
            else if((At[0] == '\r') || (At[0] == '\n'))
            {
                *At = 0;
            }
        }

        vertex *LeftVertex = GetVertexByLabel(LeftLabel, Vertices, VertexCount);
        vertex *RightVertex = GetVertexByLabel(RightLabel, Vertices, VertexCount);

        if(!LeftVertex)
        {
            Assert((VertexCount + 1) < MaxVertexCount);
            LeftVertex = CreateVertex(LeftLabel, Vertices, VertexCount++);
        }

        if(!RightVertex)
        {
            Assert((VertexCount + 1) < MaxVertexCount);
            RightVertex = CreateVertex(RightLabel, Vertices, VertexCount++);
        }

        Assert(LeftVertex);
        Assert(RightVertex);

        Assert((LeftVertex->EdgeCount + 1) < ArrayCount(LeftVertex->Edges));
        Assert((RightVertex->EdgeCount + 1) < ArrayCount(RightVertex->Edges));

        LeftVertex->Edges[LeftVertex->EdgeCount++] = RightVertex;
        RightVertex->Edges[RightVertex->EdgeCount++] = LeftVertex;
    }

    vertex *Start = 0;
    vertex *End = 0;
    for(u32 VertIndex = 0; VertIndex < VertexCount; ++VertIndex)
    {
        vertex *Vertex = Vertices + VertIndex;
        if(StringCompare(Vertex->Label, "start") == 0)
        {
            Start = Vertex;
        }
        else if(StringCompare(Vertex->Label, "end") == 0)
        {
            End = Vertex;
        }
    }
    Assert(Start);
    Assert(End);

    Start->Visited = true;

    u32 Count = 0;
    DepthFirstSearch(Start, End, Start, &Count, false);

    printf("Result: %d\n", Count);

    return(0);
}
