#include <stdio.h>
#include <limits.h>
#include "common.h"

int
main(void)
{
    char *FileName = "day01-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    s32 IncreaseCount = 0;
    s32 PreviousValue = INT_MAX;
    char ReadBuffer[32] = {0};
    while(fgets(ReadBuffer, ArrayCount(ReadBuffer), File) != 0)
    {
        s32 Value = atoi(ReadBuffer);

        if(Value > PreviousValue)
        {
            ++IncreaseCount;
        }

        PreviousValue = Value;
    }

    printf("Result: %d\n", IncreaseCount);

    return(0);
}
