#include "common.h"

struct point
{
    u8 Value;
    u8 Visited;
};

inline u32
RecursiveFloodFill(point *Points, u32 Width, u32 Height, u32 X, u32 Y)
{
    if((X < 0) || (X >= Width) ||
       (Y < 0) || (Y >= Height))
    {
        return(0);
    }

    point *Point = (Points + (Y * Width) + X);
    if(Point->Visited || Point->Value == 9)
    {
        return(0);
    }

    Point->Visited = true;

    u32 Size = 1;
    Size += RecursiveFloodFill(Points, Width, Height, X, Y - 1);
    Size += RecursiveFloodFill(Points, Width, Height, X, Y + 1);
    Size += RecursiveFloodFill(Points, Width, Height, X - 1, Y);
    Size += RecursiveFloodFill(Points, Width, Height, X + 1, Y);

    return(Size);
}

int
main(void)
{
    file_result File = ReadFile("day09-01.txt");

    memory_arena Arena_ = CreateMemoryArena(Kilobytes(32));
    memory_arena *Arena = &Arena_;

    u32 Width = 0;
    u32 Height = 0;

    u8 *At = File.Contents;
    while(*At)
    {
        if(*At == '\n')
        {
            if(Width == 0)
            {
                Width = (u32)(At - File.Contents) - 1;
            }

            ++Height;
        }
        ++At;
    }

    u32 PointMapSize = Width*Height;
    point *PointMap = PushArray(Arena, PointMapSize, point);
    point *InsertPoint = PointMap;
    At = File.Contents;
    while(*At)
    {
        if((At[0] >= '0') && (At[0] <= '9'))
        {
            InsertPoint->Value = (u8)(At[0] - '0');
            InsertPoint->Visited = false;
            ++InsertPoint;
        }
        ++At;
    }

    u32 BasinsCount = 0;
    u32 MaxBasinsCount = 500;
    u32 *Basins = PushArray(Arena, MaxBasinsCount, u32);

    point *Point = PointMap;
    for(u32 Y = 0; Y < Height; ++Y)
    {
        for(u32 X = 0; X < Width; ++X)
        {
            if(!Point->Visited && (Point->Value != 9))
            {
                Assert((BasinsCount + 1) <= MaxBasinsCount);
                Basins[BasinsCount++] = RecursiveFloodFill(PointMap, Width, Height, X, Y);
            }

            ++Point;
        }
    }

    for(u32 i = 0; i < (BasinsCount - 1); ++i)
    {
        for(u32 j = i + 1; j < BasinsCount; ++j)
        {
            if(Basins[j] >= Basins[i])
            {
                u32 Temp = Basins[j];
                Basins[j] = Basins[i];
                Basins[i] = Temp;
            }
        }
    }

    Assert(BasinsCount >= 3);
    u32 Result = Basins[0] * Basins[1] * Basins[2];
    printf("Result: %u\n", Result);

    return(0);
}
