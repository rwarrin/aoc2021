#ifndef COMMON_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <intrin.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef s32 b32;
typedef float f32;
typedef double f64;
typedef size_t sizet;
typedef uintptr_t umm;
typedef intptr_t smm;

#define Assert(Condition) do { if(!(Condition)) { *(int *)0 = 0; } } while(0);

#define InvalidCodePath Assert(!"Invalid code path");
#define InvalidDefaultCase default: { Assert(!"Invalid default case"); } break;

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

#define MAX(A, B) ((A) > (B) ? A : B)
#define MIN(A, B) ((A) < (B) ? A : B)

#define Kilobytes(Value) (Value*1024LL)
#define Megabytes(Value) (Kilobytes(Value)*1024LL)
#define Gigabytes(Value) (Megabytes(Value)*1024LL)
#define Terabytes(Value) (Gigabytes(Value)*1024LL)

inline void
ZeroSize(void *Ptr, u32 Size)
{
    u8 *At = (u8 *)Ptr;
    while(Size--)
    {
        *At++ = 0;
    }
}

inline s32
StringCompare(char *Left, char *Right)
{
    for( ; *Left == *Right; ++Left, ++Right)
    {
        if(*Left == 0)
        {
            return(0);
        }
    }
    return(*Left - *Right);
}

inline void
Copy(void *SrcIn, void *DestIn, u32 Size)
{
    u8 *Src = (u8 *)SrcIn;
    u8 *Dest = (u8 *)DestIn;
    while(Size--)
    {
        *Dest++ = *Src++;
    }
}

inline void
StringCopy(void *SrcIn, void *DestIn, u32 Size)
{
    u8 *Src = (u8 *)SrcIn;
    u8 *Dest = (u8 *)DestIn;
    while(Size--)
    {
        *Dest++ = *Src++;
        if(*Src == 0)
        {
            break;
        }
    }
}

struct memory_arena
{
    s32 Size;
    s32 Used;
    u8 *Base;
};

inline memory_arena
CreateMemoryArena(s32 Size)
{
    memory_arena Result = {0};

    Result.Size = Size;
    Result.Used = 0;
    Result.Base = (u8 *)malloc(Size);

    Assert(Result.Base);

    //ZeroSize(Result.Base, Result.Size);
    memset(Result.Base, 0x00, Result.Size);

    return(Result);
}

inline void
DestroyArena(memory_arena *Arena)
{
    if(Arena)
    {
        free(Arena->Base);
        Arena->Base = 0;

        Arena->Size = 0;
        Arena->Used = 0;
    }
}

#define PushStruct(Arena, type, ...) (type *)ArenaPush_(Arena, sizeof(type), ## __VA_ARGS__)
#define PushArray(Arena, count, type, ...) (type *)ArenaPush_(Arena, sizeof(type)*(count), ## __VA_ARGS__)
#define PushSize(Arena, size, ...) ArenaPush_(Arena, size, ## __VA_ARGS__) 

inline void *
ArenaPush_(memory_arena *Arena, s32 Size, u32 Alignment = 4)
{
    // NOTE(rick): Alignment must be a power of 2
    Assert((Alignment & (Alignment - 1)) == 0);

    void *Result = 0;

    umm AlignmentMask = Alignment - 1;
    umm ResultBase = (umm)(Arena->Base + Arena->Used);
    umm AlignmentPadding = 0;
    if(ResultBase & AlignmentMask)
    {
        umm AlignmentAmount = ResultBase & AlignmentMask;
        AlignmentPadding = Alignment - AlignmentAmount;
        Size += (s32)AlignmentPadding;
    }


    Assert((Arena->Used + Size) <= Arena->Size);
    if((Arena->Used + Size) <= Arena->Size)
    {
        Result = (u8 *)ResultBase + AlignmentPadding;
        Arena->Used += Size;
    }

    return(Result);
}

struct file_result
{
    u8 *Contents;
    u32 FileSize;
};

// TODO(rick): Needs testing
inline file_result
ReadFile(char *FileName)
{
    file_result Result = {0};

    FILE *File = fopen(FileName, "rb");
    if(File)
    {
        fseek(File, 0, SEEK_END);
        Result.FileSize = ftell(File);
        fseek(File, 0, SEEK_SET);

        Result.Contents = (u8 *)malloc(Result.FileSize + 1);
        if(Result.Contents)
        {
            fread(Result.Contents, Result.FileSize, 1, File);
            Result.Contents[Result.FileSize] = 0;
        }
        else
        {
            Result.FileSize = 0;
            Result.Contents = 0;
        }
    }

    return(Result);
}

inline f32
AbsoluteValue(f32 Value)
{
    f32 Result = Value;
    if(Value < 0.0f)
    {
        Result = -Result;
    }
    return(Result);
}

inline s32
AbsoluteValue(s32 Value)
{
    s32 Result = Value;
    if(Value < 0)
    {
        Result = -Result;
    }
    return(Result);
}

/**
 * NOTE(rick):
 * To use the debug counters paste these lines at the end of the your mine file.
 *      const u32 DebugRecordCount = __COUNTER__;
 *      debug_record DebugRecords[DebugRecordCount + 1];
 * 
 * To create a timed region of code insert the follow line in your code.
 *      TIMED_BLOCK();
 * 
 * To view counter data call PrintDebugCounters(); after all of the
 * TIMED_BLOCK() regions have been closed/exited.
 *      PrintDebugCounters();
 **/

struct debug_record
{
    u64 CycleCount;
    u32 HitCount;
    char *FileName;
    char *FunctionName;
    char *Note;
    u32 LineNumber;
};

#ifdef ENABLE_DEBUG

debug_record DebugRecords[];
extern const u32 DebugRecordCount;

#define TIMED_BLOCK__(Number, ...) debug_block DebugBlock_##Number(__COUNTER__, __FILE__, __FUNCTION__, Number, ## __VA_ARGS__)
#define TIMED_BLOCK_(Number, ...) TIMED_BLOCK__(Number, ## __VA_ARGS__)
#define TIMED_BLOCK(...) TIMED_BLOCK_(__LINE__, ## __VA_ARGS__)
struct debug_block
{
    u32 ID;
    u64 StartTick;

    debug_block(u32 Counter, char *FileName, char *FunctionName, u32 LineNumber, char *Note = 0)
    {
        ID = Counter;

        debug_record *Entry = DebugRecords + ID;
        Entry->FileName = FileName;
        Entry->FunctionName = FunctionName;
        Entry->LineNumber = LineNumber;
        Entry->Note = Note;
        ++Entry->HitCount;

        StartTick = __rdtsc();
    }

    ~debug_block()
    {
        debug_record *Entry = DebugRecords + ID;
        _InterlockedExchangeAdd64((__int64 volatile *)&Entry->CycleCount, (long)(__rdtsc() - StartTick));
    }
};

#include <windows.h>
inline void
PrintDebugCounters()
{
    OutputDebugStringA("-FUNCTION------------------------LINE-+-CYCLES------+-HITS----+-CY/HIT-------+-NOTE-----------------------------\n");

    char Buffer[256] = {0};
    for(u32 DebugIndex = 0; DebugIndex < DebugRecordCount; ++DebugIndex)
    {
        debug_record *Record = DebugRecords + DebugIndex;
        snprintf(Buffer, ArrayCount(Buffer),
                 "%-32s(%04d)| %10llucy|%8uh|%10llucy/h| %-32s\n",
                 Record->FunctionName, Record->LineNumber,
                 Record->CycleCount, Record->HitCount,
                 Record->CycleCount / Record->HitCount,
                 Record->Note ? Record->Note : "");
        OutputDebugStringA(Buffer);
    }

    OutputDebugStringA("--------------------------------------+-------------+---------+--------------+-----------------------------------\n");
}

inline u64
Win32GetWallClock()
{
    LARGE_INTEGER Result = {0};
    QueryPerformanceCounter(&Result);
    return(Result.QuadPart);
}

inline f64
Win32GetWallClockTimeElapsed(u64 Start, u64 End)
{
    LARGE_INTEGER Frequency = {0};
    QueryPerformanceFrequency(&Frequency);

    u64 TicksElapsed = End - Start;
    f64 Seconds = (f64)TicksElapsed / (f64)Frequency.QuadPart;
    return(Seconds);
}

#define AsMS(Seconds) ((Seconds)*1000.0f)
#define AsUS(Seconds) (AsMS(Seconds)*1000.0f)

inline void
PrintDebugString(char *Format, ...)
{
    va_list List;
    va_start(List, Format);

    char TempBuffer[256] = {0};
    vsnprintf(TempBuffer, ArrayCount(TempBuffer), Format, List);

    OutputDebugStringA(TempBuffer);
}

#else
#define TIMED_BLOCK(...)
#define PrintDebugCounters(...)
#define Win32GetWallClock(...) 0
#define Win32GetWallClockTimeElapsed(...) 0
#define AsMS(...) 0
#define AsUS(...) 0
#define PrintDebugString(...)
#endif

#define COMMON_H
#endif
