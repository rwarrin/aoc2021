#include "common.h"

struct v2
{
    s32 x;
    s32 y;
};

int
main(void)
{
    char *FileName = "day02-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading\n", FileName);
        return(-1);
    }

    v2 Position = {0, 0};

    char Direction[16] = {0};
    s32 Amount = 0;
    while(fscanf(File, "%s %d", Direction, &Amount) == 2)
    {
        if(Direction[0] == 'f')
        {
            Position.x += Amount;
        }
        else if(Direction[0] == 'd')
        {
            Position.y += Amount;
        }
        else if(Direction[0] == 'u')
        {
            Position.y -= Amount;
        }
    }

    s32 Result = Position.x * Position.y;
    printf("Result: %d\n", Result);

    return(0);
}
