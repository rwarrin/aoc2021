#include "common.h"

struct bit_count
{
    s32 Zero;
    s32 One;
};

int
main(void)
{
    char *FileName = "day03-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    bit_count Bits[12] = {0};

    char ReadBuffer[16] = {0};
    while(fgets(ReadBuffer, ArrayCount(ReadBuffer), File) != 0)
    {
        ReadBuffer[0] == '1' ? ++Bits[11].One : ++Bits[11].Zero;
        ReadBuffer[1] == '1' ? ++Bits[10].One : ++Bits[10].Zero;
        ReadBuffer[2] == '1' ? ++Bits[9].One : ++Bits[9].Zero;
        ReadBuffer[3] == '1' ? ++Bits[8].One : ++Bits[8].Zero;
        ReadBuffer[4] == '1' ? ++Bits[7].One : ++Bits[7].Zero;
        ReadBuffer[5] == '1' ? ++Bits[6].One : ++Bits[6].Zero;
        ReadBuffer[6] == '1' ? ++Bits[5].One : ++Bits[5].Zero;
        ReadBuffer[7] == '1' ? ++Bits[4].One : ++Bits[4].Zero;
        ReadBuffer[8] == '1' ? ++Bits[3].One : ++Bits[3].Zero;
        ReadBuffer[9] == '1' ? ++Bits[2].One : ++Bits[2].Zero;
        ReadBuffer[10] == '1' ? ++Bits[1].One : ++Bits[1].Zero;
        ReadBuffer[11] == '1' ? ++Bits[0].One : ++Bits[0].Zero;
    }

    u32 GammaRate = 0;
    u32 BitValue = 1;
    for(s32 Index = 0; Index < ArrayCount(Bits); ++Index)
    {
        bit_count *Bit = Bits + Index;
        s32 CommonBit = Bit->One > Bit->Zero ? 1 : 0;

        GammaRate += CommonBit * BitValue;

        BitValue *= 2;
    }

    u32 EpsilonRate = ~GammaRate & 0x0FFF;
    u32 Result = GammaRate*EpsilonRate;
    printf("Result: %u\n", Result);

    return(0);
}
