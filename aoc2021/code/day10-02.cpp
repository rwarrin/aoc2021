#include "common.h"

inline b32
IsChunkOpener(char C)
{
    b32 Result = ((C == '(') ||
                  (C == '[') ||
                  (C == '{') ||
                  (C == '<'));
    return(Result);
}

inline b32
IsChunkCloser(char C)
{
    b32 Result = ((C == ')') ||
                  (C == ']') ||
                  (C == '}') ||
                  (C == '>'));
    return(Result);
}

inline char
GetChunkCloserFromOpener(char C)
{
    char Result = 0;
    switch(C)
    {
        case '(': { Result = ')'; } break;
        case '[': { Result = ']'; } break;
        case '{': { Result = '}'; } break;
        case '<': { Result = '>'; } break;
        InvalidDefaultCase;
    }
    return(Result);
}

int
main(void)
{
    char *FileName = "day10-01.txt";
    file_result File = ReadFile(FileName);

    const u32 MaxLineCount = 256;
    u32 LineCount = 0;
    u8 *Lines[MaxLineCount] = {0};

    Lines[LineCount++] = File.Contents;
    for(u8 *At = File.Contents; *At; ++At)
    {
        if((At[0] == '\r') || (At[0] == '\n'))
        {
            if(At[0] == '\r')
            {
                *At++ = 0;
            }
            *At++ = 0;

            if(*At)
            {
                Lines[LineCount++] = At;
            }
        }
    }

    s32 StackSize = 0;
    const s32 MaxStackSize = 128;
    char Stack[MaxStackSize] = {0};

    for(u32 LineIndex = 0; LineIndex < LineCount; ++LineIndex)
    {
        StackSize = 0;
        ZeroSize(Stack, sizeof(char)*MaxStackSize);

        for(u8 *At = Lines[LineIndex]; *At; ++At)
        {
            if(IsChunkOpener(At[0]))
            {
                Assert((StackSize + 1) < MaxStackSize);
                Stack[StackSize++] = *At;
            }
            else if(IsChunkCloser(At[0]))
            {
                Assert(StackSize > 0);
                char Closer = GetChunkCloserFromOpener(Stack[--StackSize]);
                if(At[0] != Closer)
                {
                    u8 *Temp = Lines[LineIndex];
                    Lines[LineIndex] = Lines[--LineCount];
                    Lines[LineCount] = Temp;
                    --LineIndex;
                    break;
                }
            }
        }
    }

    u32 ScoreCount = 0;
    u64 Scores[MaxLineCount] = {0};

    for(u32 LineIndex = 0; LineIndex < LineCount; ++LineIndex)
    {
        StackSize = 0;
        ZeroSize(Stack, sizeof(char)*MaxStackSize);

        for(u8 *At = Lines[LineIndex]; *At; ++At)
        {
            if(IsChunkOpener(At[0]))
            {
                Assert((StackSize + 1) < MaxStackSize);
                Stack[StackSize++] = *At;
            }
            else if(IsChunkCloser(At[0]))
            {
                Assert(StackSize > 0);
                char Closer = GetChunkCloserFromOpener(Stack[--StackSize]);
                Assert(At[0] == Closer);
            }
        }

        u64 CompletionScore = 0;
        for(s32 StackIndex = StackSize - 1; StackIndex >= 0; --StackIndex)
        {
            u64 Value = 0;
            switch(Stack[StackIndex])
            {
                case '(': { Value = 1; } break;
                case '[': { Value = 2; } break;
                case '{': { Value = 3; } break;
                case '<': { Value = 4; } break;
                InvalidDefaultCase;
            }
            Assert(Value);

            CompletionScore = (CompletionScore * 5) + Value;
        }
        
        Scores[ScoreCount++] = CompletionScore;
    }

    for(u32 I = 0; I < ScoreCount - 1; ++I)
    {
        for(u32 J = I + 1; J < ScoreCount; ++J)
        {
            if(Scores[I] > Scores[J])
            {
                u64 Temp = Scores[I];
                Scores[I] = Scores[J];
                Scores[J] = Temp;
            }
        }
    }

    u64 Middle = (ScoreCount / 2);
    u64 MiddleScore = Scores[Middle];

    printf("Result %llu\n", MiddleScore);

    return(0);
}
