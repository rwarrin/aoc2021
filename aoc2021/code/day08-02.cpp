#include "common.h"

struct log_entry
{
    char Segments[10][8];
    u8 SegmentLengths[10];
    char Values[4][8];
    u8 ValueLengths[10];
};

inline b32
IsWhitespace(char C)
{
    b32 Result = ( (C == ' ') ||
                   (C == '\r') ||
                   (C == '\n') ||
                   (C == 't') );
    return(Result);
}

inline void
ConsumeWhitespace(char **Ptr)
{
    char *At = *Ptr;
    while(IsWhitespace(*At))
    {
        ++At;
    }
    *Ptr = At;
}

inline u8
StringToBits(char *String)
{
    u8 Result = 0;
    while(*String)
    {
        Result |= (1 << ((*String) - 'a'));
        ++String;
    }
    return(Result);
}

int
main(void)
{
    char *FileName = "day08-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to read %s\n", FileName);
        return(-1);
    }

    memory_arena Arena_ = CreateMemoryArena(Kilobytes(64));
    memory_arena *Arena = &Arena_;

    u32 LogEntriesCount = 0;
    u32 MaxLogEntriesCount = 100;
    log_entry *LogEntries = PushArray(Arena, MaxLogEntriesCount, log_entry);

    char LineBuffer[256] = {0};
    while(fgets(LineBuffer, ArrayCount(LineBuffer), File) != 0)
    {
        log_entry *Entry = LogEntries + LogEntriesCount++;

        char *At = LineBuffer;

        for(u32 SegmentIndex = 0; SegmentIndex < ArrayCount(Entry->Segments); ++SegmentIndex)
        {
            ConsumeWhitespace(&At);
            sscanf(At, " %s ", Entry->Segments + SegmentIndex);

            char *SegmentBegin = At;
            while(!IsWhitespace(*At))
            {
                ++At;
            }
            Entry->SegmentLengths[SegmentIndex] = (u8)(At - SegmentBegin);
        }

        ConsumeWhitespace(&At);
        Assert(*At == '|');
        ++At;  // NOTE(rick): Pipe
        ConsumeWhitespace(&At);

        for(u32 ValueIndex = 0; ValueIndex < ArrayCount(Entry->Values); ++ValueIndex)
        {
            ConsumeWhitespace(&At);
            sscanf(At, " %s ", Entry->Values + ValueIndex);

            char *ValueBegin = At;
            while(!IsWhitespace(*At))
            {
                ++At;
            }
            u8 Length = (u8)(At - ValueBegin);
            Entry->ValueLengths[ValueIndex] = Length;
        }
    }

    u32 TotalOutput = 0;
    for(u32 LogEntryIndex = 0; LogEntryIndex < LogEntriesCount; ++LogEntryIndex)
    {
        log_entry *Entry = LogEntries + LogEntryIndex;

        u8 BitPatterns[10] = {0};
        u8 BitPatternLengths[10] = {0};
        // NOTE(rick): First pass - identify known values
        for(u32 SegmentIndex = 0; SegmentIndex < ArrayCount(Entry->Segments); ++SegmentIndex)
        {
            u8 Bits = StringToBits(Entry->Segments[SegmentIndex]);
            u8 SegmentLength = Entry->SegmentLengths[SegmentIndex];
            switch(SegmentLength)
            {
                case 2: // 1
                {
                    BitPatterns[1] = Bits;
                    BitPatternLengths[1] = SegmentLength;
                } break;
                case 3: // 7
                {
                    BitPatterns[7] = Bits;
                    BitPatternLengths[7] = SegmentLength;
                } break;
                case 4: // 4
                {
                    BitPatterns[4] = Bits;
                    BitPatternLengths[4] = SegmentLength;
                } break;
                case 7: // 8
                {
                    BitPatterns[8] = Bits;
                    BitPatternLengths[8] = SegmentLength;
                } break;
                default:
                {
                    // NOTE(rick): Intentionally empty
                } break;
            };
        }

        // NOTE(rick): Second pass - Determine values based on known patterns
        for(u32 SegmentIndex = 0; SegmentIndex < ArrayCount(Entry->Segments); ++SegmentIndex)
        {
            u8 Bits = StringToBits(Entry->Segments[SegmentIndex]);
            u8 SegmentLength = Entry->SegmentLengths[SegmentIndex];
            switch(SegmentLength)
            {
                case 5: // 3
                {
                    if((Bits & BitPatterns[1]) == BitPatterns[1])
                    {
                        BitPatterns[3] = Bits;
                        BitPatternLengths[3] = SegmentLength;
                    }
                } break;
                case 6: // 0 6 9
                {
                    if((Bits & BitPatterns[4]) == BitPatterns[4])
                    {
                        BitPatterns[9] = Bits;
                        BitPatternLengths[9] = SegmentLength;
                    }
                    else if((Bits & BitPatterns[7]) == BitPatterns[7])
                    {
                        BitPatterns[0] = Bits;
                        BitPatternLengths[0] = SegmentLength;
                    }
                    else
                    {
                        BitPatterns[6] = Bits;
                        BitPatternLengths[6] = SegmentLength;
                    }
                } break;
                default:
                {
                    // NOTE(rick): Intentionally empty
                } break;
            };
        }

        // NOTE(rick): Final pass - Determine final two values
        for(u32 SegmentIndex = 0; SegmentIndex < ArrayCount(Entry->Segments); ++SegmentIndex)
        {
            u8 Bits = StringToBits(Entry->Segments[SegmentIndex]);
            u8 SegmentLength = Entry->SegmentLengths[SegmentIndex];
            switch(SegmentLength)
            {
                case 5: // 3
                {
                    if((Bits & BitPatterns[3]) != BitPatterns[3])
                    {
                        if((Bits & BitPatterns[9]) == Bits)
                        {
                            BitPatterns[5] = Bits;
                            BitPatternLengths[5] = SegmentLength;
                        }
                        else
                        {
                            BitPatterns[2] = Bits;
                            BitPatternLengths[2] = SegmentLength;
                        }
                    }
                } break;
                default:
                {
                    // NOTE(rick): Intentionally empty
                } break;
            };
        }

        // NOTE(rick): Decode values and compute result
        u32 OutputValue = 0;
        for(u32 ValuesIndex = 0; ValuesIndex < ArrayCount(Entry->Values); ++ValuesIndex)
        {
            u8 Bits = StringToBits(Entry->Values[ValuesIndex]);
            
            for(u32 BitPatternIndex = 0; BitPatternIndex < ArrayCount(BitPatterns); ++BitPatternIndex)
            {
                if(Entry->ValueLengths[ValuesIndex] == BitPatternLengths[BitPatternIndex])
                {
                    if((Bits & BitPatterns[BitPatternIndex]) == BitPatterns[BitPatternIndex])
                    {
                        OutputValue = (OutputValue * 10) + BitPatternIndex;
                        break;
                    }
                }
            }
        }

        TotalOutput += OutputValue;
    }

    printf("Result: %d\n", TotalOutput);

    return(0);
}
