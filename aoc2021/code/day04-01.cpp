#include "common.h"

struct bingo_cell
{
    u8 Number;
    u8 IsMarked;
};

#define BOARD_ROWS 5
#define BOARD_COLS 5

struct bingo_board
{
    bingo_cell Cells[BOARD_ROWS][BOARD_COLS];
};

inline b32
IsWhitespace(u8 C)
{
    b32 Result = ((C == ' ') ||
                  (C == '\t') || 
                  (C == '\r') || 
                  (C == '\n'));
    return(Result);
}

inline void
ConsumeWhitespace(u8 **Ptr)
{
    u8 *At = *Ptr;
    while(IsWhitespace(*At))
    {
        ++At;
    }

    *Ptr = At;
}

int
main(void)
{
    char *FileName = "day04-01.txt";
    file_result File = ReadFile(FileName);
    char *FileAt = (char *)File.Contents;

    memory_arena Arena = CreateMemoryArena(Kilobytes(64));

    u32 MaxRandomNumbersCount = 1000;
    u8 *RandomNumbers = PushArray(&Arena, MaxRandomNumbersCount, u8);
    u8 *InsertNumber = RandomNumbers;
    while(*FileAt != '\n')
    {
        Assert((InsertNumber - RandomNumbers) < MaxRandomNumbersCount);

        *InsertNumber++ = (u8)atoi(FileAt);
        while((FileAt[0] != ',') && (FileAt[0] != '\n'))
        {
            ++FileAt;
        }
        ++FileAt;
    }

    u32 RandomNumbersCount = (u32)(InsertNumber - RandomNumbers);

    Assert(FileAt[0] == '\n');
    ++FileAt;

    u32 MaxBingoBoardCount = 100;
    bingo_board **Boards = PushArray(&Arena, MaxBingoBoardCount, bingo_board *);
    u32 BingoBoardCount = 0;
    while(*FileAt != 0)
    {
        Assert(BingoBoardCount < MaxBingoBoardCount);
        bingo_board **BoardPtr = (Boards + BingoBoardCount++);
        bingo_board *Board = PushStruct(&Arena, bingo_board);
        *BoardPtr = Board;

        for(u32 Row = 0; Row < BOARD_ROWS; ++Row)
        {
            for(u32 Col = 0; Col < BOARD_COLS; ++Col)
            {
                bingo_cell *Cell = &Board->Cells[Row][Col];
                Cell->Number = (u8)atoi(FileAt);
                Cell->IsMarked = false;

                while(!IsWhitespace(*FileAt)) { ++FileAt; }
                while(IsWhitespace(*FileAt)) { ++FileAt; }
            }
        }
    }

    b32 FoundWinningBoard = false;
    u32 WinningBoardIndex = 0;
    u32 WinningRandomNumber = 0;
    for(u32 RandomNumberIndex = 0;
        !FoundWinningBoard && (RandomNumberIndex < RandomNumbersCount);
        ++RandomNumberIndex)
    {
        u32 RandomNumber = *(RandomNumbers + RandomNumberIndex);
        for(u32 BingoBoardIndex = 0;
            !FoundWinningBoard && (BingoBoardIndex < BingoBoardCount);
            ++BingoBoardIndex)
        {
            bingo_board *Board = *(Boards + BingoBoardIndex);

            for(u32 Row = 0; !FoundWinningBoard && (Row < BOARD_ROWS); ++Row)
            {
                for(u32 Col = 0; !FoundWinningBoard && (Col < BOARD_COLS); ++Col)
                {
                    bingo_cell *Cell = &Board->Cells[Row][Col];
                    if(Cell->Number == RandomNumber)
                    {
                        Cell->IsMarked = true;

                        if((Board->Cells[Row][0].IsMarked &&
                            Board->Cells[Row][1].IsMarked &&
                            Board->Cells[Row][2].IsMarked &&
                            Board->Cells[Row][3].IsMarked &&
                            Board->Cells[Row][4].IsMarked) ||
                           (Board->Cells[0][Col].IsMarked &&
                            Board->Cells[1][Col].IsMarked &&
                            Board->Cells[2][Col].IsMarked &&
                            Board->Cells[3][Col].IsMarked &&
                            Board->Cells[4][Col].IsMarked))
                        {
                            FoundWinningBoard = true;
                            WinningBoardIndex = BingoBoardIndex;
                            WinningRandomNumber = RandomNumber;
                        }
                    }
                }
            }
        }
    }

    u32 UnmarkedSum = 0;
    bingo_board *Board = *(Boards + WinningBoardIndex);
    for(u32 Row = 0; Row < BOARD_ROWS; ++Row)
    {
        for(u32 Col = 0; Col < BOARD_COLS; ++Col)
        {
            bingo_cell *Cell = &Board->Cells[Row][Col];
            UnmarkedSum += Cell->IsMarked ? 0 : Cell->Number;
        }
    }

    u32 Result = UnmarkedSum*WinningRandomNumber;
    printf("Result: %u\n", Result);

    return(0);
}
