#include "common.h"

int
main(void)
{
    char *FileName = "day06-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    u64 FishSpawningTimerBuckets[9] = {0};
    u32 InputDayBucket = 0;
    while(fscanf(File, "%d,", &InputDayBucket) == 1)
    {
        ++FishSpawningTimerBuckets[InputDayBucket + 1];
    }

    u32 SimulationDays = 256;
    for(u32 Day = 0; Day <= SimulationDays; ++Day)
    {
        u64 TodaysFishCount = FishSpawningTimerBuckets[0];

        for(u32 TransposeIndex = 0;
            TransposeIndex < ArrayCount(FishSpawningTimerBuckets) - 1;
            ++TransposeIndex)
        {
            FishSpawningTimerBuckets[TransposeIndex] = FishSpawningTimerBuckets[TransposeIndex + 1];
        }

        FishSpawningTimerBuckets[6] += TodaysFishCount;
        FishSpawningTimerBuckets[8] = TodaysFishCount;
    }

    u64 TotalFish = 0;
    for(u32 DayBucket = 0; DayBucket < ArrayCount(FishSpawningTimerBuckets); ++DayBucket)
    {
        TotalFish += FishSpawningTimerBuckets[DayBucket];
    }

    printf("Result: %llu\n", TotalFish);

    return(0);
}
