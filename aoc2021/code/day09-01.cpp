#include "common.h"

int
main(void)
{
    file_result File = ReadFile("day09-01.txt");

    memory_arena Arena_ = CreateMemoryArena(Kilobytes(32));
    memory_arena *Arena = &Arena_;

    u32 Width = 0;
    u32 Height = 0;

    u8 *At = File.Contents;
    while(*At)
    {
        if(*At == '\n')
        {
            if(Width == 0)
            {
                Width = (u32)(At - File.Contents) - 1;
            }

            ++Height;
        }
        ++At;
    }

    u32 HeightMapSize = Width*Height;
    u8 *HeightMap = PushArray(Arena, HeightMapSize, u8);
    u8 *InsertDigit = HeightMap;
    At = File.Contents;
    while(*At)
    {
        if((At[0] >= '0') && (At[0] <= '9'))
        {
            *InsertDigit++ = (u8)(At[0] - '0');
        }
        ++At;
    }

    u32 LowPointTotal = 0;

    u8 *Row = HeightMap;
    for(u32 Y = 0; Y < Height; ++Y)
    {
        for(u32 X = 0; X < Width; ++X)
        {
            u8 Up = 0xFF;
            u8 Down = 0xFF;
            u8 Right = 0xFF;
            u8 Left = 0xFF;

            if(Y > 0)
            {
                Up = *((Row - Width) + X);
            }

            if((Y + 1) < Height)
            {
                Down = *((Row + Width) + X);
            }

            if(X > 0)
            {
                Left = *(Row + (X - 1));
            }

            if((X + 1) < Width)
            {
                Right = *(Row + (X + 1));
            }

            u8 ThisValue = *(Row + X);

            if((ThisValue < Up) &&
               (ThisValue < Down) &&
               (ThisValue < Left) &&
               (ThisValue < Right))
            {
                LowPointTotal += ThisValue + 1;
            }
        }
        Row += Width;
    }

    printf("Result: %d\n", LowPointTotal);

    return(0);
}
