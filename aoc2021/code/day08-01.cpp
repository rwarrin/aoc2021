#include "common.h"

u32 DigitSegments[] =
{
    6, // 0
    2, // 1
    5, // 2
    5, // 3
    4, // 4
    5, // 5
    6, // 6
    3, // 7
    7, // 8
    6, // 9
};

u32 DigitHistogram[] =
{
    0, // 0
    0, // 1
    1, // 2
    1, // 3
    1, // 4
    5, // 5
    2, // 6
    1, // 7
    0, // 8
    0, // 9
};

struct log_entry
{
    char Segments[10][8];
    u8 SegmentLengths[10];
    char Values[4][8];
    u8 ValueLengths[10];
};

inline b32
IsWhitespace(char C)
{
    b32 Result = ( (C == ' ') ||
                   (C == '\r') ||
                   (C == '\n') ||
                   (C == 't') );
    return(Result);
}

inline void
ConsumeWhitespace(char **Ptr)
{
    char *At = *Ptr;
    while(IsWhitespace(*At))
    {
        ++At;
    }
    *Ptr = At;
}

int
main(void)
{
    char *FileName = "day08-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to read %s\n", FileName);
        return(-1);
    }

    memory_arena Arena_ = CreateMemoryArena(Kilobytes(64));
    memory_arena *Arena = &Arena_;

    u32 LogEntriesCount = 0;
    u32 MaxLogEntriesCount = 100;
    log_entry *LogEntries = PushArray(Arena, MaxLogEntriesCount, log_entry);

    u32 ValuesWithUniqueNumberOfSegments = 0;

    char LineBuffer[256] = {0};
    while(fgets(LineBuffer, ArrayCount(LineBuffer), File) != 0)
    {
        log_entry *Entry = LogEntries + LogEntriesCount++;

        char *At = LineBuffer;

        for(u32 SegmentIndex = 0; SegmentIndex < ArrayCount(Entry->Segments); ++SegmentIndex)
        {
            ConsumeWhitespace(&At);
            sscanf(At, " %s ", Entry->Segments + SegmentIndex);

            char *SegmentBegin = At;
            while(!IsWhitespace(*At))
            {
                ++At;
            }
            Entry->SegmentLengths[SegmentIndex] = (u8)(At - SegmentBegin);
        }

        ConsumeWhitespace(&At);
        ++At;  // NOTE(rick): Pipe
        ConsumeWhitespace(&At);

        for(u32 ValueIndex = 0; ValueIndex < ArrayCount(Entry->Values); ++ValueIndex)
        {
            ConsumeWhitespace(&At);
            sscanf(At, " %s ", Entry->Values + ValueIndex);

            char *ValueBegin = At;
            while(!IsWhitespace(*At))
            {
                ++At;
            }
            u8 Length = (u8)(At - ValueBegin);
            Entry->ValueLengths[ValueIndex] = Length;

            if((Length == 2) ||
               (Length == 3) ||
               (Length == 4) ||
               (Length == 7))
            {
                ++ValuesWithUniqueNumberOfSegments;
            }
        }
    }

    printf("Result: %u\n", ValuesWithUniqueNumberOfSegments);

    return(0);
}
