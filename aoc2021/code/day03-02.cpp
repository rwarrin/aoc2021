#include "common.h"

#define BINARY_LENGTH 12 
#define REPORT_LENGTH 1000

struct rating_filter
{
    u8 Active[REPORT_LENGTH];
    u32 ActiveCount;
};

int
main(void)
{
    char *FileName = "day03-01.txt";
    FILE *File = fopen(FileName, "rb");
    if(!File)
    {
        fprintf(stderr, "Failed to open %s for reading.\n", FileName);
        return(-1);
    }

    u8 DiagnosticReportBits[BINARY_LENGTH*REPORT_LENGTH] = {0};
    rating_filter OxygenGeneratorFilter = {0};
    rating_filter CO2ScrubberFilter = {0};

    u32 Row = 0;
    char ReadBuffer[16] = {0};
    while(fgets(ReadBuffer, ArrayCount(ReadBuffer), File) != 0)
    {
        for(u32 BitIndex = 0; BitIndex < BINARY_LENGTH; ++BitIndex)
        {
            *(DiagnosticReportBits + (BitIndex * REPORT_LENGTH) + Row) = ReadBuffer[BINARY_LENGTH - (BitIndex + 1)] - '0';
        }

        OxygenGeneratorFilter.Active[Row] = 1;
        ++OxygenGeneratorFilter.ActiveCount;

        CO2ScrubberFilter.Active[Row] = 1;
        ++CO2ScrubberFilter.ActiveCount;

        ++Row;
    }

    Assert(OxygenGeneratorFilter.ActiveCount == REPORT_LENGTH);
    Assert(CO2ScrubberFilter.ActiveCount == REPORT_LENGTH);

    for(s32 BitIndex = BINARY_LENGTH - 1; BitIndex >= 0; --BitIndex)
    {
        u16 OxygenBitCount[2] = {0};
        u16 ScrubberBitCount[2] = {0};
        for(u32 Row = 0; Row < REPORT_LENGTH; ++Row)
        {
            if(OxygenGeneratorFilter.Active[Row])
            {
                ++OxygenBitCount[*(DiagnosticReportBits + (BitIndex * REPORT_LENGTH) + Row)];
            }

            if(CO2ScrubberFilter.Active[Row])
            {
                ++ScrubberBitCount[*(DiagnosticReportBits + (BitIndex * REPORT_LENGTH) + Row)];
            }
        }

        u8 OxygenBitCriteria = (OxygenBitCount[1] >= OxygenBitCount[0] ? 1 : 0);
        u8 CO2BitCriteria = (ScrubberBitCount[0] <= ScrubberBitCount[1] ? 0 : 1);

        for(u32 Row = 0; Row < REPORT_LENGTH; ++Row)
        {
            if(OxygenGeneratorFilter.Active[Row] && (OxygenGeneratorFilter.ActiveCount > 1))
            {
                u8 MeetsCriteria = (*(DiagnosticReportBits + (BitIndex * REPORT_LENGTH) + Row) == OxygenBitCriteria);
                OxygenGeneratorFilter.Active[Row] = MeetsCriteria;
                OxygenGeneratorFilter.ActiveCount -= (!MeetsCriteria ? 1 : 0);
            }

            if(CO2ScrubberFilter.Active[Row] && (CO2ScrubberFilter.ActiveCount > 1))
            {
                u8 MeetsCriteria = (*(DiagnosticReportBits + (BitIndex * REPORT_LENGTH) + Row) == CO2BitCriteria);
                CO2ScrubberFilter.Active[Row] = MeetsCriteria;
                CO2ScrubberFilter.ActiveCount -= (!MeetsCriteria ? 1 : 0);
            }
        }
    }

    Assert(OxygenGeneratorFilter.ActiveCount == 1);
    Assert(CO2ScrubberFilter.ActiveCount == 1);

    u32 OxygenTargetIndex = 0;
    for(u32 Row = 0; Row < ArrayCount(OxygenGeneratorFilter.Active); ++Row)
    {
        if(OxygenGeneratorFilter.Active[Row])
        {
            OxygenTargetIndex = Row;
            break;
        }
    }

    u32 ScrubberTargetIndex = 0;
    for(u32 Row = 0; Row < ArrayCount(CO2ScrubberFilter.Active); ++Row)
    {
        if(CO2ScrubberFilter.Active[Row])
        {
            ScrubberTargetIndex = Row;
            break;
        }
    }

    u32 OxygenValue = 0;
    u32 ScrubberValue = 0;
    u32 BitValue = 1;
    for(u32 BitIndex = 0; BitIndex < BINARY_LENGTH; ++BitIndex)
    {
        OxygenValue += *(DiagnosticReportBits + (BitIndex * REPORT_LENGTH) + OxygenTargetIndex) * BitValue;
        ScrubberValue += *(DiagnosticReportBits + (BitIndex * REPORT_LENGTH) + ScrubberTargetIndex) * BitValue;
        BitValue *= 2;
    }

    u32 Result = OxygenValue * ScrubberValue;
    printf("Result: %u\n", Result);

    return(0);
}
